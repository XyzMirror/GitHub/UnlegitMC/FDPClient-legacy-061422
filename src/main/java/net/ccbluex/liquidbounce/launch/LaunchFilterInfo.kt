package net.ccbluex.liquidbounce.launch

@Retention(AnnotationRetention.RUNTIME)
annotation class LaunchFilterInfo(val filters: Array<EnumLaunchFilter>)
